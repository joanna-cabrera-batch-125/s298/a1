let http = require("http");
const PORT = 3000;

http.createServer( (request, response)=> {
	//uri/endpoint = resource
	//http method
	//body

	if(request.url === "/profile" && request.method === "GET"){
		response.writeHead( 200, 
			{"Content-Type": "text/plain"}
		);
		response.end("Welcome to my page");

	} else if (request.url === "/profile" && request.method === "POST"){
		response.writeHead( 200, 
			{"Content-Type": "text/plain"}
		);
		response.end("Data to be sent to the database");

	} else {
		response.writeHead( 404, 
			{"Content-Type": "text/plain"}
		);
		response.end("Request cannot be completed");
	}

	

} ).listen(PORT);

console.log(`Server is now connected to port ${PORT}`);